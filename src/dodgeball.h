#ifndef DODGEBALL_H
#define DODGEBALL_H

#include <pthread.h>

typedef void (*cleanup_f)(void *p);

typedef struct player
{
    int id;
    int alive;
    int hits;
    pthread_mutex_t lock;
    pthread_cond_t wakeup;
    cleanup_f cleanup;
} player_t;

void player_cleanup_routine(void *p);
void init_players(cleanup_f cleanup);
void player_exit(player_t *player);
void *player_start(void *player_start);
void *thrower_start(void *arg);
void spawn_players();
void spawn_throwers();
void init_game();
void destroy_game();

#endif /* DODGEBALL_H */