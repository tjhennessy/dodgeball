#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "dodgeball.h"

#define MAX_NUM_THROWERS 2
#define MAX_NUM_PLAYERS  10
#define MAX_NUM_HITS     100
#define TRUE             1
#define FALSE            0

int player_hits[MAX_NUM_PLAYERS];
int players_alive[MAX_NUM_PLAYERS];
player_t players[MAX_NUM_PLAYERS];
int players_active = 0;

pthread_mutex_t player_hits_lock;
pthread_mutex_t players_alive_lock;
pthread_mutex_t players_active_lock;

void player_cleanup_routine(void *p)
{
    player_t *player = (player_t *) p;
    printf("Player %d in cleanup_routine\n", player->id);

    // Set player to inactive
    pthread_mutex_lock(&players_alive_lock);
    players_alive[player->id] = FALSE; 
    pthread_mutex_unlock(&players_alive_lock);

    // Decrement players_active
    pthread_mutex_lock(&players_active_lock);
    players_active--;
    pthread_mutex_unlock(&players_active_lock);

    pthread_mutex_destroy(&player->lock);
    pthread_cond_destroy(&player->wakeup);
    memset(player, 0, sizeof(player_t));
}

void init_players(cleanup_f cleanup)
{
    int i;
    for (i = 0; i < MAX_NUM_PLAYERS; i++)
    {
        printf("Init player %d\n", i);
        players[i].id = i;
        players[i].alive = TRUE;
        players[i].hits = 0;
        pthread_mutex_init(&players[i].lock, NULL);
        pthread_cond_init(&players[i].wakeup, NULL);
        players[i].cleanup = cleanup;
        // Tell everyone player is alive
        players_alive[i] = TRUE;
        // Player is now ready to play
        players_active++;
    }
}

void player_exit(player_t *player)
{
    player->cleanup(player);
}

void *player_start(void *arg)
{
    pthread_detach(pthread_self());
    int *id = (int *) arg;
    printf("thread %d entering\n", players[*id].id);
    while (players[*id].hits < MAX_NUM_HITS)
    {
        // Check to see if player is last one standing
        pthread_mutex_lock(&players_alive_lock);
        if (players_active <= 1)
        {
            pthread_mutex_unlock(&players_alive_lock);
            printf("Player %d wins!\n", players[*id].id);
            break;
        }
        pthread_mutex_unlock(&players_alive_lock);

        // Wait for hit signal, spin in case of spurious wakeup
        pthread_mutex_lock(&players[*id].lock);
        while (TRUE)
        {
            pthread_cond_wait(&players[*id].wakeup, &players[*id].lock);
            pthread_mutex_lock(&player_hits_lock);
            int legit_wakeup = FALSE;
            if (player_hits[*id] > 0)
            {
                // Player was hit some number of times
                // If not hits registered, spurious wake up
                //   occurred and we keep spinning
                players[*id].hits = player_hits[*id];
                player_hits[*id] = 0; // reset count
                legit_wakeup = TRUE;  // ie not spurious
            }
            pthread_mutex_unlock(&player_hits_lock);

            if (legit_wakeup)
            {   // Allows player_hits_lock to be unlocked above
                break;
            }
        }
        pthread_mutex_unlock(&players[*id].lock);
    }

    player_exit(&players[*id]);

    free(id);

    return NULL;    
}

void *thrower_start(void *arg)
{
    printf("thrower entering\n");
    while (TRUE)
    {
        // Check if game is over, players_active == 0
        int exit_game = FALSE;
        pthread_mutex_lock(&players_active_lock);
        if (0 == players_active)
        {   // No longer need to hit players, they are not in play
            exit_game = TRUE;
        }
        pthread_mutex_unlock(&players_active_lock);

        if (exit_game)
        {
            // Fall out of loop, game is over
            break;
        }

        // Players still active, so hit 'em
        // Treat any random number that is not valid as a miss
        int player_hit = rand() % MAX_NUM_PLAYERS;
        
        pthread_mutex_lock(&players_alive_lock);
        if (players_alive[player_hit])
        {
            // Player was hit, increment their hit count
            // Must increment before signal b/c player
            //   uses this to increment their private hit count
            pthread_mutex_lock(&player_hits_lock);
            player_hits[player_hit]++;
            pthread_mutex_unlock(&player_hits_lock);

            // Signal player hit
            pthread_cond_signal(&players[player_hit].wakeup);
        }
        pthread_mutex_unlock(&players_alive_lock);
    }
}

void spawn_players()
{
    pthread_t tids[MAX_NUM_PLAYERS];
    int i;
    for (i = 0; i < MAX_NUM_PLAYERS; i++)
    {
        int *n = (int *) malloc(sizeof(int));
        if (NULL == n)
        {
            fprintf(stderr, "Error: no memory for malloc\n");
            exit(EXIT_FAILURE);
        }
        *n = i;
        pthread_create(&tids[i], NULL, player_start, n);
    }
}

void spawn_throwers()
{
    pthread_t tids[MAX_NUM_THROWERS];
    int i;
    for (i = 0; i < MAX_NUM_THROWERS; i++)
    {
        pthread_create(&tids[i], NULL, thrower_start, NULL);
    }

    for (i = 0; i < MAX_NUM_THROWERS; i++)
    {
        pthread_join(tids[i], NULL);
    }
}

void init_game()
{
    // Init mutexes
    pthread_mutex_init(&players_alive_lock, NULL);
    pthread_mutex_init(&players_active_lock, NULL);
    pthread_mutex_init(&player_hits_lock, NULL);

    // Init players
    init_players(player_cleanup_routine);
    printf("Players active: %d\n", players_active);
    
    // Start simulation
    spawn_players();  // detached threads = no join
    spawn_throwers(); // will wait to join
}

void destroy_game()
{
    printf("Exiting game, players %d\n", players_active);
    printf("Players active: %d\n", players_active);
    pthread_mutex_destroy(&players_alive_lock);
    pthread_mutex_destroy(&players_active_lock);
    pthread_mutex_destroy(&player_hits_lock);
}

int main(void)
{
    init_game();
    destroy_game();

    return 0;
}